import UIKit

//▸ Создать массив элементов 'кол-во дней в месяцах' содержащих количество дней в соответствующем месяце
let amountDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30 , 31, 30 , 31]






//▸ Создать массив элементов 'название месяцов' содержащий названия месяцев
let namesOfMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]





//▸ Используя цикл for и массив 'кол-во дней в месяцах' выведите количество дней в каждом месяце (без имен месяцев)
for amountDays in amountDaysInMonth{
    print(amountDays)
}
print("\n\n\n")






//▸ Используйте еще один массив с именами месяцев, чтобы вывести название месяца + количество дней
for counterMonth in 0..<amountDaysInMonth.count{
    print("\(namesOfMonths[counterMonth]) have a \(amountDaysInMonth[counterMonth]) days")
}
print("\n\n\n")





//▸ Сделайте тоже самое, но используя массив tuples (кортежей) с параметрами (имя месяца, кол-во дней)
var tupleMonth : [(nameMonth : String, amountDays : Int)] = []
for counterMonth in 0..<amountDaysInMonth.count{
    tupleMonth.append((namesOfMonths[counterMonth], amountDaysInMonth[counterMonth]))
}
for month in tupleMonth{
    print("\(month.nameMonth) have a \(month.amountDays) days")
}

print("\n\n\n")




//////////////////////////////////////////////////////////////////////
/// Сделайте тоже самое, только выводите дни в обратном порядке (порядок в массиве не менять)
for counter in 0..<tupleMonth.count{
    let reverseCounter = tupleMonth.count - counter - 1
    print("\(tupleMonth[reverseCounter].nameMonth) have a \(tupleMonth[reverseCounter].amountDays) days")
}
print("\n\n\n")


for month in tupleMonth.reversed(){
    print("\(month.nameMonth) have a \(month.amountDays) days")
}
print("\n\n\n\n")
//////////////////////////////////////////////////////////////////////






//▸ Для произвольно выбранной даты (месяц и день) посчитайте количество дней до этой даты от начала года
var selectedDay = (name : "November", days : 30)
var sum = 0
for month in tupleMonth {
    if month.nameMonth == selectedDay.name{
        sum += selectedDay.days
        break
    }
    else{
        sum += month.amountDays
    }
}
print(sum)
